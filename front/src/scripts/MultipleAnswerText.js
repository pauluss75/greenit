import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import green from "@material-ui/core/colors/green";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Paper from "@material-ui/core/Paper/Paper";
import Button from "@material-ui/core/Button/Button";
import Grid from "@material-ui/core/Grid/Grid";
import TextField from "@material-ui/core/TextField/TextField";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    root: {
        flexGrow: 1,
        color: green[600],
        "&$checked": {
            color: green[500]
        }
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: "center",
        color: theme.palette.text.secondary
    },
    checked: {}
});

class CheckboxLabels extends React.Component {
    state = {
        TF:"",
        one: false,
        two: false,
        third: false,
        fourth: false,
        fifth: false
    };

    handleChange = name => event => {
        this.setState({ [name]: event.target.checked });
    };

    handleChangeTF = name => event => {
        this.setState({ [name]: event.target.value });
    };

    render() {

        const { classes, questionO ,Submit, keyPress} = this.props;

        const question = questionO.question;

        const reponses = questionO.reponses;

        return (
            <div className={classes.root}>
                <Grid container spacing={24} justify="center" alignItems="center">
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>{question}</Paper>
                    </Grid>

                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                           <FormGroup row>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.one}
                                            onChange={this.handleChange("one")}
                                            value="checkedG"
                                            classes={{
                                                root: classes.root,
                                                checked: classes.checked
                                            }}
                                        />
                                    }
                                    label={reponses[0].reponse}
                                />
                            </FormGroup>
                        </Paper>
                    </Grid>


                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <FormGroup row>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.two}
                                            onChange={this.handleChange("two")}
                                            value="checkedG"
                                            classes={{
                                                root: classes.root,
                                                checked: classes.checked
                                            }}
                                        />
                                    }
                                    label={reponses[1].reponse}
                                />
                            </FormGroup>
                        </Paper>
                    </Grid>


                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <FormGroup row>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.third}
                                            onChange={this.handleChange("third")}
                                            value="checkedG"
                                            classes={{
                                                root: classes.root,
                                                checked: classes.checked
                                            }}
                                        />
                                    }
                                    label={reponses[2].reponse}
                                />
                            </FormGroup>
                        </Paper>
                    </Grid>


                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <FormGroup row>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.fourth}
                                            onChange={this.handleChange("fourth")}
                                            value="checkedG"
                                            classes={{
                                                root: classes.root,
                                                checked: classes.checked
                                            }}
                                        />
                                    }
                                    label={reponses[3].reponse}
                                />
                            </FormGroup>
                        </Paper>
                    </Grid>

                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <form className={classes.container} noValidate autoComplete="off">

                                <TextField
                                    id="standard-full-width"
                                    onChange={this.handleChangeTF("TF")}
                                    label="Click Next when you're done ;)"
                                    style={{ margin: 8 }}
                                    placeholder={reponses[4].reponse}
                                    fullWidth
                                    margin="normal"
                                    onKeyDown={keyPress}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </form>
                        </Paper>
                    </Grid>

                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <FormGroup row>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.fifth}
                                            onChange={this.handleChange("fifth")}
                                            value="checkedG"
                                            classes={{
                                                root: classes.root,
                                                checked: classes.checked
                                            }}
                                        />
                                    }
                                    label={reponses[5].reponse}
                                />
                            </FormGroup>
                        </Paper>
                    </Grid>



                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <Button size="large"
                                    color="primary"
                                    className={classes.button}
                                    onClick={Submit.bind(null, [88,
                                                                                questionO.id,
                                                                                this.state.TF,
                                                                                this.state.one,
                                                                                reponses[0].reponse,
                                                                                this.state.two,
                                                                                reponses[1].reponse,
                                                                                this.state.third,
                                                                                reponses[2].reponse,
                                                                                this.state.fourth,
                                                                                reponses[3].reponse,
                                                                                this.state.fifth,
                                                                                reponses[4].reponse])}>
                                Next
                            </Button>
                        </Paper>
                    </Grid>
                </Grid>
            </div>


        );
    }
}

CheckboxLabels.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CheckboxLabels);
