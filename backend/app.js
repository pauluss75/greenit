const express = require('express')
const iParser = require("body-parser");
const fs = require('fs')

const app = express()

const HOST = '0.0.0.0'
const PORT = 8080

app.use(iParser.json());
app.use(iParser.urlencoded({ extended: true }))
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.post('/answer', (req, res) => {
  if (req.body) {
    console.log("Body received: "+JSON.stringify(req.body))
    let db = fs.readFileSync('answers.json')

    if (db != "") {
      try {
        db = JSON.parse(db)
      } catch (e) {
        console.log(e)
      }
      db.push(req.body)
    } else {
      db = [req.body]
    }
    fs.writeFileSync('answers.json', JSON.stringify(db))
  }
  res.sendStatus(200)
})

app.listen(PORT, HOST, () => {
  console.log('Example app listening on port 3000!')
})
