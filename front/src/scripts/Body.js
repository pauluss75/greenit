import React, { Component } from 'react';

import UniqueAnswer from './UniqueAnswer'
import TextAnswer from './TextAnswer'
import Title from './Title'
import MultipleAnswer from './MultipleAnswer'
import MultipleAnswerText from './MultipleAnswerText'

import json from './questions.json';
import FinalScreen from "./FinalScreen";



class Body extends Component {

    constructor(props) {
        super(props);
        this.state = {indexQ : 0, showTitle : 0, questionTitle: "", response:"{\"tab\" : ["};
    }

    SubmitRadio = (i) => {

        let answer = "{ ";
        let flag = 0;

        answer += "\"id\" : " + i[1] + ", ";

        if (i[2]){
            flag =1;
            answer += "\"reponse\" : \"" + i[3] + "\",";
        }
        if (i[4]){
            flag =1;
            answer += "\"reponse\" : \"" + i[5] + "\",";
        }
        if (i[6]){
            flag =1;
            answer += "\"reponse\"  :\"" + i[7] + "\",";
        }
        if (i[8]){
            flag =1;
            answer += "\"reponse\" : \"" + i[9]  + "\", ";
        }

        if (flag === 0){
            answer += "\"reponse\" : null "
        }
        else{
            answer += "\"BK\" : null";
        }

        answer += " }";
        console.log (answer);

        this.state.response += answer + ",";

        this.setState({indexQ: i[0] - 1});
    };

    Click = (i) => {


        let answer = "{ ";

        answer += "\"id\" : " + i[1] + ", ";

        answer += "\"reponse\" : \"" + i[2] + "\" ";

        answer += " }";
        console.log (answer);
        this.state.response += answer + ",";

        this.setState({indexQ: i[0] - 1});
    };

    SubmitRadioText = (i) => {

        let answer = "{ ";

        answer += "\"id\" : " + i[1] + ",";

        if (i[2] === ""){
            answer += "\"TF\" : null,";
        }
        else{
            answer += "\"TF\" : \"" + i[2] + "\",";
        }

        if (i[3]){
            answer += "\"reponse\" : \"" + i[4] + "\",";
        }
        if (i[5]){
            answer += "\"reponse\" : \"" + i[6] + "\",";
        }
        if (i[7]){
            answer += "\"reponse\" : \"" + i[8] + "\",";
        }
        if (i[9]){
            answer += "\"reponse\" : \"" + i[10] + "\",";
        }
        if (i[11]){
            answer += "\"reponse\" : \"" + i[12] + "\",";
        }


        answer += "\"BK\" : null";


        answer += " }";
        console.log (answer);

        this.state.response += answer + ",";

        this.setState({indexQ: i[0] - 1});
    };

    SubmitText = (i) => {

        let answer = "{ ";

        answer += "\"id\" : " + i[1] + ", ";

        answer += "\"TF\" : \"" + i[2] + "\"";

        answer += " }";
        console.log (answer);

        this.state.response += answer + ",";

        this.setState({indexQ: i[0] - 1});
    };

    Post = (json) => {

        console.log(json)

        fetch('http://51.75.250.46:8080/answer', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: json
        });
    };

    keyPress = (e) =>{
        if(e.keyCode === 13){
            e.preventDefault();
        }
    }


    render() {

        let title;

        if (this.state.indexQ === 89) {

            this.state.response += " null ]}"

            this.Post( this.state.response);

            return (<div className="Body">
                <div className="MainCont">
                    <FinalScreen/>
                </div>
            </div>);
        }

        if (json[this.state.indexQ].reponses[0].type === "title") {

            this.setState({questionTitle: json[this.state.indexQ].question});

            switch (this.state.indexQ) {
                case 88:
                    this.setState({showTitle: 5});
                    break;
                case 17:
                    this.setState({showTitle: 12});
                    break;
                case 30:
                    this.setState({showTitle: 7});
                    break;
                case 49:
                    this.setState({showTitle: 11});
                    break;
                case 62:
                    this.setState({showTitle: 6});
                    break;
                default:
                    break;
            }

            title = (<Title
                question={this.state.questionTitle}/>);

            this.setState({indexQ: json[this.state.indexQ].reponses[0].nextQ});

        }

        let component;

        if (json[this.state.indexQ].reponses[0].type === "textField"){
            this.state.showTitle -= 1;

            if (this.state.showTitle > 0){
                title = (<Title
                    question={this.state.questionTitle}/>);
            }

            component = (<TextAnswer
                            keyPress={this.keyPress}
                            SubmitText={this.SubmitText}
                            questionO={json[this.state.indexQ]}/>);
        }
        else if (json[this.state.indexQ].reponses[0].type === "checkbox"){

            this.state.showTitle -= 1;

            if (this.state.showTitle > 0){
                title = (<Title
                    question={this.state.questionTitle}/>);
            }

            component = (<UniqueAnswer
                Post={this.Post}
                Click={this.Click}
                questionO={json[this.state.indexQ]}/>);
        }
        else if (json[this.state.indexQ].reponses[0].type === "radioButtonText"){

            this.state.showTitle -= 1;

            if (this.state.showTitle > 0){
                title = (<Title
                    question={this.state.questionTitle}/>);
            }

            component = (
                    <MultipleAnswerText
                        keyPress={this.keyPress}
                        Submit={this.SubmitRadioText}
                        questionO={json[this.state.indexQ]}/>
            );
        }

        else if (json[this.state.indexQ].reponses[0].type === "radioButton"){

            this.state.showTitle -= 1;

            if (this.state.showTitle > 0){
                title = (<Title
                    question={this.state.questionTitle}/>);
            }

            component = (<MultipleAnswer
                SubmitRadio={this.SubmitRadio}
                questionO={json[this.state.indexQ]}/>);
        }

        return (
            <div className="Body">
               <div className="MainCont">
                    {title}
                    {component}
               </div>
            </div>
        );
    }
}

export default Body;
