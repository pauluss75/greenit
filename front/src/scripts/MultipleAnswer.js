import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button/Button";
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import green from "@material-ui/core/colors/green";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    root: {
        flexGrow: 1,
        color: green[600],
        "&$checked": {
            color: green[500]
        }
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: "center",
        color: theme.palette.text.secondary
    },
    checked: {}
});


class Checkboxs extends React.Component {


    state = {
        one: false,
        two: false,
        third: false,
        fourth: false,
        fifth: false
    };

    handleChange = name => event => {
        this.setState({[name]: event.target.checked});
    };

    render() {

        const {classes, SubmitRadio, questionO} = this.props;

        const question = questionO.question;

        const reponses = questionO.reponses;

        return (
            <div className={classes.root}>
                <Grid container spacing={24} justify="center" alignItems="center">
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>{question}</Paper>
                    </Grid>


                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <FormGroup row>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.one}
                                            onChange={this.handleChange("one")}
                                            value="checkedG"
                                            classes={{
                                                root: classes.root,
                                                checked: classes.checked
                                            }}
                                        />
                                    }
                                    label={reponses[0].reponse}
                                />
                            </FormGroup>
                        </Paper>
                    </Grid>


                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <FormGroup row>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.two}
                                            onChange={this.handleChange("two")}
                                            value="checkedG"
                                            classes={{
                                                root: classes.root,
                                                checked: classes.checked
                                            }}
                                        />
                                    }
                                    label={reponses[1].reponse}
                                />
                            </FormGroup>
                        </Paper>
                    </Grid>


                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <FormGroup row>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.third}
                                            onChange={this.handleChange("third")}
                                            value="checkedG"
                                            classes={{
                                                root: classes.root,
                                                checked: classes.checked
                                            }}
                                        />
                                    }
                                    label={reponses[2].reponse}
                                />
                            </FormGroup>
                        </Paper>
                    </Grid>


                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <FormGroup row>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.fourth}
                                            onChange={this.handleChange("fourth")}
                                            value="checkedG"
                                            classes={{
                                                root: classes.root,
                                                checked: classes.checked
                                            }}
                                        />
                                    }
                                    label={reponses[3].reponse}
                                />
                            </FormGroup>
                        </Paper>
                    </Grid>

                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <Button size="large"
                                    color="primary"
                                    className={classes.button}
                                    onClick={
                                        SubmitRadio.bind(null,
                                [reponses[0].nextQ,
                                        questionO.id,
                                        this.state.one,
                                        reponses[0].reponse,
                                        this.state.two,
                                        reponses[1].reponse,
                                        this.state.third,
                                        reponses[2].reponse,
                                        this.state.fourth,
                                        reponses[3].reponse])}>
                                Next
                            </Button>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

Checkboxs.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Checkboxs);
