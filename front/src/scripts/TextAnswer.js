

import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button/Button";
import TextField from "@material-ui/core/TextField/TextField";
const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: "center",
        color: theme.palette.text.secondary
    },
});

class CheckboxLabels extends React.Component {
    state = {
        TF:""
    };

    handleChangeTF = name => event => {
        this.setState({ TF: event.target.value });
    };


    render() {

        const { classes, SubmitText, questionO, keyPress} = this.props;

        const question = questionO.question;

        const reponses = questionO.reponses;

        return (
            <div className={classes.root}>
                <Grid container spacing={24} justify="center" alignItems="center">
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>{question}</Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <form className={classes.container} noValidate autoComplete="off">
                                <TextField
                                    id="standard-full-width"
                                    onChange={this.handleChangeTF("TF")}
                                    label="Click Next when you're done ;)"
                                    style={{ margin: 8 }}
                                    placeholder="Type here"
                                    fullWidth
                                    margin="normal"
                                    onKeyDown={keyPress}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </form>
                        </Paper>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <Button size="large"
                                    color="primary"
                                    className={classes.button}
                                    onClick={SubmitText.bind(null, [reponses[0].nextQ, questionO.id, this.state.TF])}>
                                Next
                            </Button>
                        </Paper>
                    </Grid>
                </Grid>
            </div>


        );
    }
}

CheckboxLabels.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CheckboxLabels);











