import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button/Button";

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: "center",
        color: theme.palette.text.secondary
    },
});

function CenteredGrid(props) {

    const { classes, Click, questionO} = props;

    const question = questionO.question;

    const reponses = questionO.reponses;

    return (
        <div className={classes.root}>
            <Grid container spacing={24} justify="center" alignItems="center">
                <Grid item xs={12}>
                    <Paper className={classes.paper}>{question}</Paper>
                </Grid>
                {reponses.map(function(response, index){
                    return <Grid key={index} item xs={8}>
                        <Paper className={classes.paper}>
                            <Button size="large"
                                    color="primary"
                                    className={classes.button}
                                    onClick={Click.bind(null, [response.nextQ, questionO.id, response.reponse])}>
                                - {response.reponse} -
                            </Button>
                        </Paper>
                    </Grid>;
                })}
            </Grid>
        </div>
    );
}

CenteredGrid.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CenteredGrid);
